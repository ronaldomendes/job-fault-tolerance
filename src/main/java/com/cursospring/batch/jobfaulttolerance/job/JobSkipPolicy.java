package com.cursospring.batch.jobfaulttolerance.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

@Slf4j
public class JobSkipPolicy implements SkipPolicy {

    // when the return of SkipPolicy method is TRUE all job operations is able to continue until the end
    // but if you set the return with FALSE value your job operations will stop showing an error
    @Override
    public boolean shouldSkip(Throwable t, int skipCount) throws SkipLimitExceededException {
        log.error("Skip policy error: {} | Caused by: {}. Skip count: {}", t.getMessage(), t.getCause().getMessage(), skipCount);
        return true;
    }
}
