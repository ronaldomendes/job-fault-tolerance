package com.cursospring.batch.jobfaulttolerance.mapper;

import com.cursospring.batch.jobfaulttolerance.dto.EmployeeDTO;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import static com.cursospring.batch.jobfaulttolerance.utils.Constants.AGE;
import static com.cursospring.batch.jobfaulttolerance.utils.Constants.EMAIL;
import static com.cursospring.batch.jobfaulttolerance.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.jobfaulttolerance.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.jobfaulttolerance.utils.Constants.LASTNAME;

public class EmployeeFileRowMapper implements FieldSetMapper<EmployeeDTO> {

    @Override
    public EmployeeDTO mapFieldSet(FieldSet fieldSet) throws BindException {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setEmployeeId(fieldSet.readString(EMPLOYEE_ID));
        dto.setFirstName(fieldSet.readString(FIRSTNAME));
        dto.setLastName(fieldSet.readString(LASTNAME));
        dto.setEmail(fieldSet.readString(EMAIL));
        dto.setAge(fieldSet.readInt(AGE));
        return dto;
    }
}
