# Spring Batch: Fault Tolerance using SkipPolicy

In this example you will learn how to create a job with Fault Tolerance. 

This is a way to protect your job execution from incompatible and unparseable CSV content using Spring Batch architecture.